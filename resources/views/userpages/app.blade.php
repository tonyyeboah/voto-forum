<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voto Forum</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
 -->

	<title>Voto-Forum</title>


	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

<style type="text/css">
	.head{font-size: 30px; color: #888888; font-family: verdana;}
	.col-md-8{}
	.col-md-4{}
	a{font-size: 20px;}
	ul{list-style-type: none;}
	p{font-size: 14px; margin-bottom: ;}
	
</style>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="#" style="font-size:40px">Voto-Forum</a>
				
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{url('userpages/index')}}">Home</a></li>
				</ul>
               
				<ul class="nav navbar-nav navbar-right">

					@if (!Session::get('user'))
						<li><a href="login">Login</a></li>
						<li><a href="register">Register</a></li>
					@else

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Session::get('user') }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								@if(Session::get('user_type') =="Expert")
									<li><a href="{{ url('userpages/expert') }}">Dashboard</a></li>
								@elseif(Session::get('user_type') =="Student")
									<li><a href="{{ url('userpages/student') }}">Dashboard</a></li>
								@endif
								<li><a href="{{ url('userpages/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript">
			$(document).ready(function(){
				$('#select_all').on('click',function(){
					if ($(this).is(':checked')) {
						$('.chkbox').each(function(){
							this.checked = true;
						});
					}else{
						$('.chkbox').each(function(){
							this.checked = false;
						});
					}
				})
				});

</script>
</body>
</html>
