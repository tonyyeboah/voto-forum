@extends('userpages.app')
<?php session_start(); ?>
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			

				<div class="head"  style="margin-left:150px;">Read Full Questions</div>

	     </div>
	</div>
		
    


   <div class="row">
   	 <div class="col-md-8">

   	  <legend>Question</legend>
             @foreach($results as $result)
               <div class="form-group">
                  <input type="hidden" value={{Session::get('user_id')}} name="answer_id">
                        <input type="hidden" value=1 name="question_id">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                  <h3>{{ $result->title}}</h3>
               </div>

               <div class="form-group">
                  
                  <p>{{$result->question}}</p>
                   <div style="margin-top:100px;">
                     <legend>Answers</legend>
                     <p>{{$result->answer}}</p>

                     <p class="pull-right">
                     Answered by:{{$result->fullname}}<br/>
                     {{$result->updated_at}}

                     </p>
                   </div>
               </div>

               @endforeach
               
               
                 
               
                  
    <div class="col-md-4">
    	

    </div>


   </div>
				
			

</div>
@endsection
