@extends('userpages.app')
<?php session_start(); ?>
@section('content')


<div class="container">
	<div class="row">
		<div class="col-sm-6">

          <h2>Questions to answer</h2>
            @if(!$results)
              <h4>No questions to answer</h4>
            @else
 
              <ol>
                @foreach ($results as $result)
                    <a href="{{ url('userpages/answer',$result->question_id)}}">
                       <li>{{$result->title}} </li>
                     </a>
                @endforeach
              </ol>
            @endif

        </div>

		<div class="col-sm-6">

          <h2>Questions Answered</h2>
            @if(!$answeredLists)
              <h4>No answered questions </h4>
            @else
 
              <ol>
                @foreach ($answeredLists as $answeredList)
                    <a href="{{ url('userpages/readfull',$answeredList->question_id)}}">
                       <li>{{$answeredList->title}} </li>
                     </a>
                @endforeach

              </ol>
            @endif

		</div>



    </div>




</div>




@endsection