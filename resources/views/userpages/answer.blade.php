@extends('userpages.app')
<?php session_start(); ?>
@section ('content')




<div class="container">
	<div class="row">
		<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-offset-2">
			<form action="{{url('userpages/answer')}}" method="POST" role="form">
					<legend>Question</legend>
				 @foreach($results as $result)
					<div class="form-group">
						<input type="hidden" value={{Session::get('user_id')}} name="answer_id">
                        <input type="hidden" value={{$result->question_id }} name="question_id">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<h3>{{ $result->title}}</h3>
					</div>

					<div class="form-group">
						<label for="content">Content</label>
						<p>{{$result->question}}</p>

					</div>

					@endforeach
					
						<div class="form-group">
							<label for="answer">Answer</label>
							<textarea name="answer" id="inputAnswer" class="form-control" rows="3" required="required"></textarea>
						</div>
					
						
					
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>

					
				</form>	
		</div>	
	</div>
</div>

















@endsection
