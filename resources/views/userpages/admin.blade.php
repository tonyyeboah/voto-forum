@extends('userpages/app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			

				<div class="head"  style="">Newly Registered Experts </div>

	     </div>
	</div>
		
    


   <div class="row">
   	 <div class="col-md-8">


   	  <table class="table"  style="width:500px;" >
            <tr>
               <form action="{{url('userpages/admin/all')}}" method="POST" role="form">
               <th> Names of Experts</th>
               <td><input type="checkbox" name="verify" id="select_all">
               <input type="hidden" name="_token" value="{{ csrf_token() }}"></td>
               <td></td>
               <td><button type="submit" class="btn btn-primary  btn-sm">Verify all</button></td>
               </form>
            </tr>
           
           
                @foreach($results as $result) 
                <form action="{{url('userpages/admin')}}" method="POST" role="form">
                  <tr>
                     
                        <th>{{$result->fullname}}</th>
                        <td>
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <input type="hidden"  name="user_id" value="{{$result->user_id}}">
                           <input type="checkbox" name="verify" class="chkbox">
                        
                           
                        </td>
                        <td>{{$result->user_status}}</td>
                        <td><button type="submit" class="btn btn-primary btn-sm">Verify</button></td>
                        
                  </tr>
               </form>
               @endforeach 
               <tr>
               <th></th>
               
            </tr>
               
            
             
            </table>
   	 </div>


    <div class="col-md-4" style="height:500px; background-color:#888888;">
    	

    </div>


   </div>
				
			

</div>

@endsection
