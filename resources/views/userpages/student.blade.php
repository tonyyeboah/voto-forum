@extends('userpages.app')

@section('content')


<div class="container">
	<div class="row">
		<div class="col-sm-6">

                            @if(Session::has('info'))
                              <p class="alert alert-info">{{ Session::get('info') }}</p>
                             @endif

          <h2>Post a question</h2>
            <form action="student" method="POST" role="form">
            	
                 <input type="hidden" value={{ Session::get('user_id')}} name="questioner_id">
                 <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            	<div class="form-group">
            		<label for="title">Title</label>
            		<input name="title"type="text" class="form-control" id="tile" placeholder="Input field">
            	</div>
            	<div class="form-group">
            		<label for="question">Content</label>
            		<textarea name="question" id="inputContent" class="form-control" rows="3" required="required"></textarea>
            	</div>
                <div class="form-group">
                    <label for="question">Expert</label>
                    <select name="expert" id="inputExpert" class="form-control" required="required">
                      @foreach ($results as $result)
                        <option value="{{$result->user_id}}">{{$result->fullname}}</option>
                      @endforeach
                       
                    </select>
                </div>

            
            	
            
            	<button type="submit" class="btn btn-primary">Submit Question</button>
            </form>
		</div>

		<div class="col-sm-6">

          
              

              @if(Session::has('message'))
       <p class="alert alert-info">{{ Session::get('message') }}</p>
             @endif
          <h2>
          
          Posted Questions
          </h2>
         @if(!$questions)
              <h4>No questions posted</h4>
            @else
 
              <ol>
                @foreach ($questions as $question)
                    <a href="{{ url('userpages/readfull',$question->question_id)}}">
                       <li>{{$question->title}} </li>
                     </a>
                @endforeach
              </ol>
            @endif
           <h2>
          
          Answered Questions
          </h2>
          @if(!$answeredLists)
              <h4>No questions </h4>
            @else
 
              <ol>
                @foreach ($answeredLists as $answeredList)
                    <a href="{{ url('userpages/readfull',$answeredList->question_id)}}">
                       <li>{{$answeredList->title}} </li>
                     </a>
                @endforeach

              </ol>
            @endif


		</div>



    </div>




</div>




@endsection