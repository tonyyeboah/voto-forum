<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questions', function(Blueprint $table)
		{
			$table->increments('question_id');
			$table->integer('questioner_id')->unsigned();
			$table->foreign('questioner_id')->references('user_id')->on('users');
			$table->string('title');
			$table->longText('question');
			$table->integer('expert_id')->unsigned();
			$table->foreign('expert_id')->references('user_id')->on('users');
			
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questions');
	}

}
