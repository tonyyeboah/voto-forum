�]q (}q(X   staticqX   1qX   argsq]qX   docqX�  /**
 * CRON expression parser that can determine whether or not a CRON expression is
 * due to run, the next run date and previous run date of a CRON expression.
 * The determinations made by this class are accurate if checked run once per
 * minute (seconds are dropped from date time comparisons).
 *
 * Schedule parts must map to:
 * minute [0-59], hour [0-23], day of month, month [1-12|JAN-DEC], day of week
 * [1-7|MON-SUN], and an optional year.
 *
 * @link http://en.wikipedia.org/wiki/Cron
 */qX   kindqX   varq	X   returnsq
X    qX   classqX   CronExpressionqX
   visibilityqX   publicqX   nameqX   MINUTEqX
   implementsqhX   pathqXW   C:\xampp\htdocs\Voto-forum\vendor\mtdowling\cron-expression\src\Cron\CronExpression.phpqX   extendsqhu}q(hhh]qhhhh	h
hhhhhhX   HOURqhhhhhhu}q(hhh]qhhhh	h
hhhhhhX   DAYqhhhhhhu}q(hhh]qhhhh	h
hhhhhhX   MONTHqhhhhhhu}q(hhh]q hhhh	h
hhhhhhX   WEEKDAYq!hhhhhhu}q"(hhh]q#hhhh	h
hhhhhhX   YEARq$hhhhhhu}q%(hX   0q&h]q'hX3   /**
     * @var array CRON expression parts
     */q(hh	h
X   arrayq)hhhX   privateq*hX
   $cronPartsq+hhhhhhu}q,(hh&h]q-hX7   /**
     * @var FieldFactory CRON field factory
     */q.hh	h
X   FieldFactoryq/hhhh*hX   $fieldFactoryq0hhhhhhu}q1(hhh]q2(]q3(X   $expressionq4X   stringq5e]q6(X   $fieldFactoryq7X   FieldFactoryq8eehXB   /**
     * @var array Order in which to test of cron parts
     */q9hh	h
X   arrayq:hhhh*hX   $orderq;hhhhhhu}q<(hhhh2hX�  /**
     * Factory method to create a new CronExpression.
     *
     * @param string $expression The CRON expression to create.  There are
     *                           several special predefined values which can be used to substitute the
     *                           CRON expression:
     *
     *      `@yearly`, `@annually` - Run once a year, midnight, Jan. 1 - 0 0 1 1 *
     *      `@monthly` - Run once a month, midnight, first of month - 0 0 1 * *
     *      `@weekly` - Run once a week, midnight on Sun - 0 0 * * 0
     *      `@daily` - Run once a day, midnight - 0 0 * * *
     *      `@hourly` - Run once an hour, first minute - 0 * * * *
     * @param FieldFactory $fieldFactory Field factory to use
     *
     * @return CronExpression
     */q=hX   funcq>h
X   CronExpressionq?hhhhhX   factoryq@hhhhhhu}qA(hh&h]qB(]qC(X   $expressionqDX   stringqEe]qF(X   $fieldFactoryqGX   FieldFactoryqHeehX�   /**
     * Parse a CRON expression
     *
     * @param string       $expression   CRON expression (e.g. '8 * * * *')
     * @param FieldFactory $fieldFactory Factory to create cron fields
     */qIhh>h
hhhhhhX   __constructqJhhhhhhu}qK(hh&h]qL]qM(X   $valueqNX   stringqOeahX�   /**
     * Set or change the CRON expression
     *
     * @param string $value CRON expression (e.g. 8 * * * *)
     *
     * @return CronExpression
     * @throws \InvalidArgumentException if not a valid CRON expression
     */qPhh>h
X   CronExpressionqQhhhhhX   setExpressionqRhhhhhhu}qS(hh&h]qT(]qU(X	   $positionqVX   intqWe]qX(X   $valueqYX   stringqZeehX(  /**
     * Set part of the CRON expression
     *
     * @param int    $position The position of the CRON expression to set
     * @param string $value    The value to set
     *
     * @return CronExpression
     * @throws \InvalidArgumentException if the value is not valid for the part
     */q[hh>h
X   CronExpressionq\hhhhhX   setPartq]hhhhhhu}q^(hh&h]q_(]q`(X   $currentTimeqahe]qb(X   $nthqcX   intqde]qe(X   $allowCurrentDateqfX   boolqgeehX  /**
     * Get a next run date relative to the current date or a specific date
     *
     * @param string|\DateTime $currentTime      Relative calculation date
     * @param int              $nth              Number of matches to skip before returning a
     *                                           matching next run date.  0, the default, will return the current
     *                                           date and time if the next run date falls on the current date and
     *                                           time.  Setting this value to 1 will skip the first match and go to
     *                                           the second match.  Setting this value to 2 will skip the first 2
     *                                           matches and so on.
     * @param bool             $allowCurrentDate Set to TRUE to return the current date if
     *                                           it matches the cron expression.
     *
     * @return \DateTime
     * @throws \RuntimeException on too many iterations
     */qhhh>h
hhhhhhX   getNextRunDateqihhhhhhu}qj(hh&h]qk(]ql(X   $currentTimeqmhe]qn(X   $nthqoX   intqpe]qq(X   $allowCurrentDateqrX   boolqseehX;  /**
     * Get a previous run date relative to the current date or a specific date
     *
     * @param string|\DateTime $currentTime      Relative calculation date
     * @param int              $nth              Number of matches to skip before returning
     * @param bool             $allowCurrentDate Set to TRUE to return the
     *                                           current date if it matches the cron expression
     *
     * @return \DateTime
     * @throws \RuntimeException on too many iterations
     * @see Cron\CronExpression::getNextRunDate
     */qthh>h
hhhhhhX   getPreviousRunDatequhhhhhhu}qv(hh&h]qw(]qx(X   $totalqyX   intqze]q{(X   $currentTimeq|he]q}(X   $invertq~X   boolqe]q�(X   $allowCurrentDateq�X   boolq�eehXD  /**
     * Get multiple run dates starting at the current date or a specific date
     *
     * @param int              $total            Set the total number of dates to calculate
     * @param string|\DateTime $currentTime      Relative calculation date
     * @param bool             $invert           Set to TRUE to retrieve previous dates
     * @param bool             $allowCurrentDate Set to TRUE to return the
     *                                           current date if it matches the cron expression
     *
     * @return array Returns an array of run dates
     */q�hh>h
X   arrayq�hhhhhX   getMultipleRunDatesq�hhhhhhu}q�(hh&h]q�]q�(X   $partq�X   stringq�eahXj  /**
     * Get all or part of the CRON expression
     *
     * @param string $part Specify the part to retrieve or NULL to get the full
     *                     cron schedule string.
     *
     * @return string|null Returns the CRON expression, a part of the
     *                     CRON expression, or NULL if the part was specified but not found
     */q�hh>h
X   stringq�hhhhhX   getExpressionq�hhhhhhu}q�(hh&h]q�hXq   /**
     * Helper method to output the full expression.
     *
     * @return string Full CRON expression
     */q�hh>h
X   stringq�hhhhhX
   __toStringq�hhhhhhu}q�(hh&h]q�]q�(X   $currentTimeq�heahX  /**
     * Determine if the cron is due to run based on the current date or a
     * specific date.  This method assumes that the current number of
     * seconds are irrelevant, and should be called once per minute.
     *
     * @param string|\DateTime $currentTime Relative calculation date
     *
     * @return bool Returns TRUE if the cron is due to run or FALSE if not
     */q�hh>h
X   boolq�hhhhhX   isDueq�hhhhhhu}q�(hh&h]q�(]q�(X   $currentTimeq�he]q�(X   $nthq�X   intq�e]q�(X   $invertq�X   boolq�e]q�(X   $allowCurrentDateq�X   boolq�eehX_  /**
     * Get the next or previous run date of the expression relative to a date
     *
     * @param string|\DateTime $currentTime      Relative calculation date
     * @param int              $nth              Number of matches to skip before returning
     * @param bool             $invert           Set to TRUE to go backwards in time
     * @param bool             $allowCurrentDate Set to TRUE to return the
     *                                           current date if it matches the cron expression
     *
     * @return \DateTime
     * @throws \RuntimeException on too many iterations
     */q�hh>h
hhhhX	   protectedq�hX
   getRunDateq�hhhhhhu}q�(hh&h]q�hhhhh
hhhhhhhhhhhhhue.