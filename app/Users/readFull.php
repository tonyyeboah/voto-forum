<?php namespace App\Users;

use Hash;

use Auth;
use Session;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class  extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['fullname', 'email', 'password','expertise','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = ['password', 'remember_token'];


	public static function addRecord($posted){


	// public static $rules=[

 //    'fullname'=>'required',
 //    'password'=>'required',
 //    'email'   =>'required|email',
 //    'expertise'=>'required',
 //    'status'=>'required',

	// ];

		return User::create([ 
					'fullname' => $posted['fullname'],
					'email' => $posted['email'],
					'password' =>Hash::make( $posted['password']),
					'expertise' => $posted['expertise'],
					                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
					
				
			]);
	}

}


