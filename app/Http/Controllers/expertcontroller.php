<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\postanswer;
use App\postquestion;
use Auth;
use Redirect;
use DB;
use Session;
use Request;
//use Illuminate\Http\Request;

class expertcontroller extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$user=Session::all();
		
		return view('userpages/expert')->with('user', $user);
	}
	public function answer()
	{
		//
		$user=Session::all();
		//Session::put('user',$user->fullname);
		return view('userpages/answer')->with('user', $user);
	}

	public function showQuestions()
	{
		# code...
		$user=Session::all();
		$results = DB::select('select * from questions where expert_id = ? and question_status = ?', [Session::get('user_id'), 'not_answered']);
		$answeredLists = DB::select('select * from answers join questions on(answers.answer_id=questions.expert_id) where  answer_id = ? and questions.question_status = ?', [Session::get('user_id'),'answered']);
		
		return view('userpages/expert')->with('results',$results)->with('answeredLists',$answeredLists);
	}

	/*public function showQuestionsAnswered()
	{
		# code... select * from answers join questions on(answers.answer_id=questions.expert_id) where  answer_id=4
		$user=Session::all();
		$answeredLists = DB::select('select * from answers join questions on(answers.answer_id=questions.expert_id) where  answer_id = ?', [Session::get('user_id')]);
		return view('userpages/expert')->with('answeredLists',$answeredLists);
	}*/



	public function questionDetails($id){
     $results = DB::select('select * from questions where question_id = ?', [$id]);

	
	return view('userpages/answer',compact('results'));
}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$answer=Request::all();

		postanswer::create(['answer_id' => $answer['answer_id'],
									  'question_id' => $answer['question_id'],
									  'answer' => $answer['answer']]);

		DB::select('update questions SET question_status ="answered" where question_id = ?', [$answer['question_id']]);
		
        
		 return Redirect::to('userpages/expert');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
