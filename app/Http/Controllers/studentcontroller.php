<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\postquestion;
use Auth;
use DB;
use Redirect;
use Session;
use Request;

//use Illuminate\Http\Request;

class StudentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$user=Session::all();

		$results = DB::select('select * from questions where expert_id = ? and question_status = ?', [Session::get('user_id'), 'answered']);
		
		return view('userpages.student')->with('user', $user)->with('results', $results);

		                                

		return view('userpages.student')->with('user', $user);

	}

	public function question()
	{
		//
		$user=Session::all();
		return view('userpages/question')->with('user', $user);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStudent()
	{
		//
		$question =Request::all();
        
		postquestion::create(['questioner_id' => $question['questioner_id'],
									  'title' => $question['title'],
									  'question' => $question['question'],
									  'expert_id' => $question['expert']]);
		
		return Redirect:: to('userpages/student')->with('info','Question successfully posted');
		                                           

		return view('userpages/student');


		return Redirect::to('userpages/student');


	}

	public function getExperts()
	{
		# code...

          $results = DB::select('select * from users where expertise like ?', ['expert']);


       //  $results = DB::select('select * from users where expertise like ?', ['expert']);

          $results = DB::select('select * from users where expertise like ? and user_status like ?', ['expert','verified']);
          $questions = DB::select('select * from questions where questioner_id = ? and question_status = ?', [Session::get('user_id'), 'not answered']);
		  $answeredLists = DB::select('select * from answers join questions on(answers.answer_id=questions.expert_id and questions.question_id=answers.question_id) where  questioner_id = ? and questions.question_status = ?', [Session::get('user_id'),'answered']);


          return view('userpages/student')->with('results',$results)->with('questions',$questions)->with('answeredLists',$answeredLists);
	}



	public function showQuestions()
	{
		# code...
		$user=Session::all();
		$results = DB::select('select * from questions where questioner_id = ? and question_status = ?', [Session::get('user_id'), 'not answered']);
		$answeredLists = DB::select('select * from answers join questions on(answers.answer_id=questions.expert_id) where  answer_id = ? and questions.question_status = ?', [Session::get('user_id'),'answered']);
		
		return view('userpages/expert')->with('results',$results)->with('answeredLists',$answeredLists);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
