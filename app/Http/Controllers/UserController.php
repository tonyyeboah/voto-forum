<?php namespace App\Http\Controllers;
use Input;
use Redirect;
use Auth;
use Session;

//use Illuminate\Routing\Controller;
use DB;
use App\Users\User;
use App\Users\Question;
use App\postanswer;
use App\postquestion;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */


  public function index(){

  	          $results=Question::getQuestions();

		      return view('userpages.index',compact('results'));
         }


public function readfull($id)
	{
        $results= DB::select('select * from (answers,users) join questions on(answers.question_id=questions.question_id and users.user_id=questions.expert_id) where  questions.question_id = ? and questions.question_status = ?', [$id,'answered']);
		return view('userpages/readfull',compact('results'));
        

	

		
	}

public function login()
	{
		return view('userpages.login');
	}


	public function PostLogin(){


		$email = Input::get('email');
		$password = Input::get('password');
	


		$errorMessage = array('error' =>'Incorrect email/Password');

   if (Auth::attempt(['email' => $email, 'password' => $password]))
        {

        	$results = DB::select('select * from users where email = ? ', [$email] );
		    $expertise = $results[0]->expertise;

        	$user=Auth::user();


         

		 Session::put('user',$user->fullname);
		 Session::put('user_id',$user->user_id );
		 Session::put('user_type', $user->expertise);
        $results = DB::select('select * from users where email = ?', [$email]);
		$expertise = $user->expertise;

		if (strcasecmp($expertise, 'student') ==0) {
			# code...
		 return Redirect::to('userpages/student')/*->with('user', $user)*/;

		}

		elseif (strcasecmp($expertise, 'expert') ==0) {
			# code...
			return Redirect::to('userpages/expert');
		}
        
         
		}

	
		else{



			        return Redirect::to('userpages/login')->with('errors', $errorMessage);
		}
            
	}



public function register()
	{

		return view('userpages.register');
	}


public function postRegister()
 {

	    if(! User::isValid(Input::all())){


	       return Redirect::back()->withInput()->withErrors(User::$messages);


  	       $posted = Input::get();

	  	      }

	  	      
	  	     
           $posted = Input::get();

  	       
		   $ok = User::addRecord($posted);

		   return Redirect::to('userpages/login')->with('message','Successfully Registered! Please Login');
		                                           
}
   
   public function admin()
	{   
		$results = DB::select('select * from users where expertise like ?', ['expert']);
		return view('userpages.admin')->with('results',$results);
	}

	public function adminVerify()
	{
		# code...
		$user= Input::get();
		if($user['verify']){
				DB::select('update users SET user_status ="verified" where user_id = ?', [$user['user_id']]);
		}
      
		return Redirect::to('userpages/admin');
       // dd($user);
	}

	public function adminVerifyAll()
	{
		# code...
		$user= Input::get();
		if($user['verify']){
				DB::select('update users SET user_status ="verified" where expertise like ?', ['expert']);
		}

      
		return Redirect::to('userpages/admin');
       // dd($user);
	}

	 public function logout()
	{
		Session::flush();
		return Redirect::to('userpages/login');
	}




public function dispalyQuestions(){

		

}

// public function i()
// 	{
// 		return view('home');
// 	}

}
