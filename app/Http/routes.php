<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

//<<<<<<< Updated upstream
//Route::get('home', 'HomeController@index');
//=======
//Route::get('home', 'HomeController@index');

Route::get('userpages/student',array('as'=>'student','uses'=>'StudentController@index'));
Route::get('userpages/student',array('as'=>'student','uses'=>'StudentController@getExperts'));
Route::post('userpages/student',array('as'=>'StudentQuestion','uses'=>'StudentController@postStudent'));
//Route::get('student/question', 'StudentController@question');
Route::get('userpages/question',array('as'=>'StudentQuestion','uses'=>'StudentController@question'));

//Route::get('expert', 'ExpertController@index');
Route::get('userpages/expert',array('as'=>'expert','uses'=>'ExpertController@index'));
Route::get('userpages/expert',array('as'=>'expert','uses'=>'ExpertController@showQuestions'));
//Route::get('userpages/expert',array('as'=>'expert','uses'=>'ExpertController@showQuestionsAnswered'));

Route::get('userpages/logout',array('as'=>'logout','uses'=>'UserController@logout'));

//Route::get('expert/answer', 'ExpertController@answer');
Route::get('userpages/answer/{id}',array('as'=>'answer','uses'=>'ExpertController@questionDetails'));
Route::get('userpages/answer',array('as'=>'answer','uses'=>'ExpertController@answer'));

Route::post('userpages/answer',array('as'=>'answer','uses'=>'ExpertController@store'));


//>>>>>>> Stashed changes

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('userpages/index',array('as'=>'index','uses'=>'UserController@index'));
//<<<<<<< Updated upstream


//Route::get('userpages/readfull',array('as'=>'readfull','uses'=>'UserController@readfull'));

Route::get('userpages/readfull/{id}',array('as'=>'readfull','uses'=>'UserController@readfull'));
Route::get('userpages/login',array('as'=>'login','uses'=>'UserController@login'));
Route::post('userpages/login',array('as'=>'login','uses'=>'UserController@postLogin'));
Route::get('userpages/register',array('as'=>'register','uses'=>'UserController@register'));
Route::post('userpages/register',array('as'=>'register','uses'=>'UserController@postRegister'));
Route::get('userpages/admin',array('as'=>'admin','uses'=>'UserController@admin'));
Route::post('userpages/admin',array('as'=>'admin','uses'=>'UserController@adminVerify'));
Route::post('userpages/admin/all',array('as'=>'admin','uses'=>'UserController@adminVerifyAll'));



Route::get('userpages/paginaton',array('as'=>'paginate','uses'=>'UserController@pagination'));
