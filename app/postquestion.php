<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class postquestion extends Model {

	//
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'questions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['questioner_id','title' , 'question','expert_id'];

	/*public static function create($value)
	{
		//{"question_id":"1","questioner_id":"1","_token":"bUzlEBKjGEUZ67J7H18uYmMdzEdPC17UYaik23gR","title":"Assignment 1","question":"a","expert":"1"}
		return $questionInfo = array('questioner_id' => $value['questioner_id'],
									  'title' => $value['title'],
									  'question' => $value['question'],
									  'expert_id' => $value['expert']);
	}*/
}
